# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains the scripts that are used to create and configure websites of my clients ([Canadian Institute of International Business](https://ciib.org), [DSMD](https://dariaseramd.com), [EPPM](https://projectmanagementmethodology.com), others)
* They include everything that is needed to setup both Debian and Ubuntu, configure security tools (such as fail2ban) and install web environment

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact